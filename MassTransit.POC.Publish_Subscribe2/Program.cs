﻿using GreenPipes;
using MassTransit.POC.Messages;
using System;
using System.Threading.Tasks;

namespace MassTransit.POC.Publish_Subscribe

{
    public class Program
    {

        public static void Main()
        {

            var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
            {

                var host = sbc.Host(new Uri("rabbitmq://localhost"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });

                sbc.ReceiveEndpoint(host, "Message1Sample_queue", ep =>
                {
                    //handle message subscription 2 to check round robin and scalability
                    ep.Handler<IMessage1Sample>(context =>
                    {
                        return Console.Out.WriteLineAsync($"Received: {context.Message.Text} {context.Message.Id}");
                    });
                });
            });


            bus.Start();

            Console.WriteLine("Press any key to exit Subscribe 2");
            Console.ReadKey();

            bus.Stop();
        }
    }
}

