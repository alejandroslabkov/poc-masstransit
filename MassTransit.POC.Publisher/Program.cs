﻿using System;
using System.Threading.Tasks;
using MassTransit.POC.Messages;
using RabbitMQ.Client;

namespace MassTransit.POC.Publisher
{
    class Program
    {
        static void Main(string[] args)
        {
            var bus = Bus.Factory.CreateUsingRabbitMq(config =>
            {
                config.ExchangeType = ExchangeType.Fanout;

                var host = config.Host(new Uri("rabbitmq://shatlsvc01.rehashed.corp/Test"), h =>
                {
                    h.Username("test");
                    h.Password("test");
                });
            });

            bus.Start();

            //publish n messages
            for (int i = 0; i < 10; i++)
            {
                bus.Publish<IMessage1Sample>(new {Text = $"Test {i}", Id = Guid.NewGuid()});


                //this is to test request-response type
                var client = bus.CreateRequestClient<IRequestResponseSample, IRequestResponseSample>(new Uri("rabbitmq://shatlsvc01.rehashed.corp/Test/requestResponseSample_queue"), TimeSpan.FromSeconds(10));


                Task.Run(async () =>
                {
                    IRequestResponseSample response = await client.Request(new { Text = "Hola" });
                    Console.WriteLine($"Id: {response.Id} {response.Text}");
                }).Wait();

            }

            Console.WriteLine("Press any key to exit Publisher");
            Console.ReadKey();

            bus.Stop();
        }
    }
}
