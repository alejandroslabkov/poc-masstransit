﻿using System;

namespace MassTransit.POC.Messages
{
    public interface IMessage1Sample
    {
        string Text { get; set; }
        Guid Id { get; set; }
    }

    public interface IRequestResponseSample
    {
        string Text { get; set; }
        Guid Id { get; set; }
    }
}
