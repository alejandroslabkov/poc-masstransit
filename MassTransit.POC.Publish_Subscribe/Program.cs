﻿using GreenPipes;
using MassTransit.POC.Messages;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace MassTransit.POC.Publish_Subscribe

{

    public class Program
    {

        public static readonly ConcurrentDictionary<int, int> valuesRetry = new ConcurrentDictionary<int, int>();

        public static void Main(string[] args)
        {
            var id = args[0];
            //Console.WriteLine(id);

            var bus = Bus.Factory.CreateUsingRabbitMq(config =>
            {
                //sbc.UseRetry(retryConfig => retryConfig.Immediate(10));

                var host = config.Host(new Uri("rabbitmq://shatlsvc01.rehashed.corp/Test"), h =>
                {
                    h.Username("test");
                    h.Password("test");
                });

                config.ReceiveEndpoint(host, $"Message1Sample_queue", ep =>
                {
                    ep.AutoDelete = false;
                    ep.Durable  = true;
                    ep.SetQueueArgument("x-expires", null);

                    //handle message subscription
                    ep.Handler<IMessage1Sample>(context =>
                    {
                        //Console.Out.WriteLineAsync($"Consuming: {context.Message.Text} {context.Message.Id}");

                        //random error - if we always fire the exception finally it will go to the fault handler
                        if (new Random().Next(1, 11) > 5)
                            throw new Exception("FORCED ERROR");

                        return Console.Out.WriteLineAsync($"Received: {context.Message.Text} {context.Message.Id}");
                    });

                    //this is to handle fault messages, for example after 5 replies with no success
                    ep.Handler<Fault<IMessage1Sample>>(context =>
                    {

                        var originalMessage = context.Message.Message;
                        var exceptions = context.Message.Exceptions;

                        //try re-publish
                        context.Publish<IMessage1Sample>(context.Message.Message);

                        return Console.Out.WriteLineAsync($"Error for: {originalMessage.Id} {exceptions[0].Message}");
                    });

                });
            });

            bus.Start();

            ////publish n messages
            //for (int i = 0; i < 10; i++)
            //    bus.Publish<IMessage1Sample>(new { Text = $"Test {i}", Id = Guid.NewGuid() });

            ////this is to test request-response type
            //var client = bus.CreateRequestClient<IRequestResponseSample, IRequestResponseSample>(new Uri("rabbitmq://localhost/requestResponseSample_queue"), TimeSpan.FromSeconds(10));

            //Task.Run(async () =>
            //{
            //    IRequestResponseSample response = await client.Request(new { Text = "Hola" });
            //    Console.WriteLine("Id: {0}", response.Id);
            //}).Wait();

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();

            bus.Stop();
        }
    }
}

