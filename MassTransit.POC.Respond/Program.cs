﻿using MassTransit.POC.Messages;
using System;
using System.Threading.Tasks;

namespace MassTransit.POC.Respond
{
    class Program
    {
        static void Main(string[] args)
        {
            var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
            {
                var host = sbc.Host(new Uri("rabbitmq://shatlsvc01.rehashed.corp/Test"), h =>
                {
                    h.Username("test");
                    h.Password("test");
                });

                sbc.ReceiveEndpoint(host, "requestResponseSample_queue",
                    e => e.Consumer<RequestConsumer>());
            });

            bus.Start();

            Console.WriteLine("Press any key to exit Respond Proccess");
            Console.ReadKey();
        }
    }

    public class RequestConsumer : IConsumer<IRequestResponseSample>
    {
        private static string _id;

        static RequestConsumer()
        {
            _id = Guid.NewGuid().ToString();
        }

        public async Task Consume(ConsumeContext<IRequestResponseSample> context)
        {
            //adding id
            context.Message.Id = Guid.NewGuid();
            context.Message.Text += $"from consumer {_id}";

            context.Respond(context.Message);
        }

    }
}
